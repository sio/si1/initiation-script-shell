#!/bin/bash
SOMME=0
while true
do
    echo -n "saisir un nombre (vide pour arrêter) : "
    read NOMBRE
    if [ -z ${NOMBRE} ]
    then
        echo "la somme des nombres saisis vaut : ${SOMME}"
      exit 0
    else
        SOMME=$(expr ${SOMME} + ${NOMBRE})
    fi
done
