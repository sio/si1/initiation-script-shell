#!/bin/bash
if [ "$1" ]
then
    echo "PID du programme $1"
    ps -C $1 -o pid=
else
    echo "Aucun nom de programme n'a été fourni" >&2
    exit 1
fi
